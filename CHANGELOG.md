# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [Unreleased]
### Changed

## 0.1.0 - 2019-07-06
### Added
- Basic typeahead component and matchers.

[Unreleased]: https://gitlab.com/cshclm/simql-typeahead/compare/HEAD...0.1.0
[0.1.0]: https://gitlab.com/cshclm/simql-typeahead/compare/0.1.0...722476fc8318e8d95e5ea341c4490cb293189c6d
