(ns bitpattern.simql.typeahead.impl.util.trie-test
  (:require [bitpattern.simql.typeahead.impl.util.trie :as sut]
            [cljs.test :refer [deftest testing is are] :include-macros true]))

(defn ->no-context
  [term]
  {:term term :props {}})

(defn ->some-context
  [term]
  {:term term
   :props {:context "abc"}})

(deftest word-index-test
  (testing "Basic indexing"
    (testing "should index a single word"
      (is (= (sut/phrase-index sut/empty-trie ["hello"])
             {"h" {"e" {"l" {"l" {"o" {:props {}}}}}}})))

    (testing "should index two overlapping words"
      (is (= (sut/phrase-index sut/empty-trie ["hello" "help"])
             {"h" {"e" {"l" {"l" {"o" {:props {}}}
                             "p" {:props {}}}}}})))

    (testing "should index identically regardless of insertion order."
      (is (= (sut/phrase-index sut/empty-trie ["help" "hello"])
             {"h" {"e" {"l" {"l" {"o" {:props {}}}
                             "p" {:props {}}}}}})))

    (testing "should allow multiple indexations."
      (is (= (sut/phrase-index
              (sut/phrase-index sut/empty-trie ["help" "hello"])
              ["hello!"])
             {"h" {"e" {"l" {"l" {"o" {:props {}
                                       "!" {:props {}}}}
                             "p" {:props {}}}}}})))

    (testing "should index two words without overlap."
      (is (= (sut/phrase-index sut/empty-trie ["wildlife" "hello"])
             {"w" {"i" {"l" {"d" {"l" {"i" {"f" {"e" {:props {}}}}}}}}}
              "h" {"e" {"l" {"l" {"o" {:props {}}}}}}})))

    (testing "should do blank string insertion should a longer string be defined."
      (is (= (sut/phrase-index sut/empty-trie ["hello" "hell"])
             {"h" {"e" {"l" {"l" {"o" {:props {}}
                                  :props {}}}}}})))

    (testing "should maintain the definition of a shorter string when defining a longer one."
      (is (= (sut/phrase-index sut/empty-trie ["hell" "hello"])
             {"h" {"e" {"l" {"l" {"o" {:props {}}
                                  :props {}}}}}})))

    (testing "should be case insensitive."
      (is (= (sut/phrase-index sut/empty-trie ["TRAP"])
             {"t" {"r" {"a" {"p" {:props {}}}}}})))

    (testing "should produce the expected result for a series of similar words."
      (is (= (sut/phrase-index sut/empty-trie ["the" "that" "then" "than" "hen" "thin"])
             {"h" {"e" {"n" {:props {}}}}
              "t" {"h" {"e" {:props {}
                             "n" {:props {}}}
                        "i" {"n" {:props {}}}
                        "a" {"n" {:props {}}
                             "t" {:props {}}}}}}))))

  (testing "Context-aware indexing"
    (testing "should assign correct contexts where there are similar terms."
      (is (= (sut/phrase-index* sut/empty-trie [(->no-context "help")
                              (->some-context "hello")])
             {"h" {"e" {"l" {"l" {"o" {:props {:context "abc"}}}
                             "p" {:props {}}}}}})))

    (testing "should assign correct contexts where there are existing substrings"
      (is (= (sut/phrase-index* sut/empty-trie [(->no-context "hell")
                              (->some-context "hello")])
             {"h" {"e" {"l" {"l" {"o" {:props {:context "abc"}}
                                  :props {}}}}}})))

    (testing "should assign correct contexts when a substring exists"
      (is (= (sut/phrase-index* sut/empty-trie [(->no-context "hello")
                              (->some-context "hell")])
             {"h" {"e" {"l" {"l" {:props {:context "abc"}
                                  "o" {:props {}}}}}}})))

    (testing "should produce the expected result for a series of similar words."
      (is (= (sut/phrase-index* sut/empty-trie (concat
                                                      (map ->no-context ["the" "that" "then"])
                                                      (map ->some-context ["than" "hen" "thin"])))
             {"h" {"e" {"n" {:props {:context "abc"}}}}
              "t" {"h" {"e" {:props {}
                             "n" {:props {}}}
                        "i" {"n" {:props {:context "abc"}}}
                        "a" {"n" {:props {:context "abc"}}
                             "t" {:props {}}}}}})))))

(deftest phrase-index-test
  (testing "Should index phrases"
    (is (= (sut/phrase-index* sut/empty-trie (map ->no-context ["hello: world"]))
           {"h" {"e" {"l" {"l" {"o" {":" {" " {"w" {"o" {"r" {"l" {"d" {:props {}}}}}}}}}}}}}}))))

(deftest complete-test
  (testing "Should find a single match in a one-term trie."
    (is (= (sut/complete {"h" {"e" {"l" {"l" {"o" {:props {}}}}}}} "hel")
           ["hello"])))

  (testing "Should support multiple completions in a simple trie."
    (is (= (sut/complete {"h" {"e" {"l" {"l" {:props {}
                                             "o" {:props {}}}}}}} "hel")
           ["hell" "hello"])))

  (testing "Should support multiple completions in a simple trie."
    (is (= (sut/complete {"h" {"e" {"l" {"l" {:props {}
                                             "o" {:props {}}}}}}} "hel")
           ["hell" "hello"])))

  (testing "Results are sorted alphabetically."
    (is (= (sut/complete {"x" {"y" {"z" {:props {}}}}
                         "a" {"b" {"c" {"e" {:props {}}
                                        "d" {:props {}}}}}} "")
           ["abcd" "abce" "xyz"])))

  (testing "Should be able to numerous completions in a more complex trie."
    (is (= ["hen" "than" "that" "the" "then" "thin"]
           (sut/complete {"h" {"e" {"n" {:props {}}}}
                          "t" {"h" {"e" {:props {}
                                         "n" {:props {}}}
                                    "i" {"n" {:props {}}}
                                    "a" {"n" {:props {}}
                                         "t" {:props {}}}}}}
                         ""))))

  (testing "Should complete phrases"
    (is (= (sut/complete
            {"h" {"e" {"l" {"l" {"o" {":" {" " {"w" {"o" {"r" {"l" {"d" {:props {}}}}}}}}}}}}}}
            "hello")
           ["hello: world"]))))
