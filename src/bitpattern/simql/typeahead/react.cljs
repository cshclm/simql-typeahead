(ns bitpattern.simql.typeahead.react
  (:require [bitpattern.simql.typeahead.core :as simql-ta]
            [bitpattern.simql.typeahead.completion :as completion]
            [reagent.core :as r]))

(def ^:export TypeaheadInput
  "Like `typeahead-input`, but for React."
  (r/reactify-component
   (fn [props]
     [simql-ta/typeahead-input
      (update props :completion-item-base #(some-> % r/adapt-react-class))])))

(defn- ctx->clj
  [ctx]
  (-> (js->clj ctx :keywordize-keys true)
      (update :type keyword)
      (update :context (fn [c]
                         (when c
                           (js->clj c :keywordize-keys true))))))

(defn ^:export complete
  [idx x]
  (clj->js (completion/complete idx (js->clj x))))

(defn ^:export prop
  [idx ctx k]
  (completion/prop idx (ctx->clj ctx) (keyword k)))

(defn ^:export indexCompletion
  [idx ctx completions]
  (js/console.log (ctx->clj ctx))
  (js/console.log (js->clj completions))
  (let [v (completion/index! idx (ctx->clj ctx) (js->clj completions))]
    (js/console.log v)
    v))

(defn ^:export setProps
  [idx ctx props]
  (completion/set-props! idx (ctx->clj ctx) (js->clj props)))

(def ^:export create completion/create)
