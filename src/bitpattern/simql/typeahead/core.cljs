(ns bitpattern.simql.typeahead.core
  "Reagent (React) component for typeahead."
  (:require [bitpattern.simql.typeahead.completion :as completion]
            [bitpattern.simql.typeahead.impl.components.typeahead :as typeahead]
            [bitpattern.simql.typeahead.impl.components.input-field :as input-field]
            [bitpattern.simql.typeahead.impl.components.completion-list :as completion-list]
            [bitpattern.simql.typeahead.impl.components.completion-item :as completion-item]
            [bitpattern.simql.typeahead.impl.util.memo :as memo]
            [reagent.core :as r]))

(def ^:private default-operator ":")

(defn- complete
  [state idx]
  (if-let [s @state]
    (completion/complete idx s)))

(def ^:private make-on-change-handler
  (memo/with-memo
    (fn [state on-change]
      (fn [query ctx]
        (reset! state (update ctx
                              :type #(if (= % :string-search)
                                       :field
                                       %)))
        (when on-change
          (on-change query ctx))))))

(def ^:private make-on-context-change-handler
  (memo/with-memo
    (fn [state on-context-change]
      (fn [ctx]
        (reset! state ctx)
        (when on-context-change
          (on-context-change ctx))))))

(defn typeahead-input
  "A typeahead input component for Reagent.

  This requires the following props:

  * `idx`: the completion index to use. See `bitpattern.simql.typeahead.completion`.
  * `query`: the current query string

  It may also take any of the following props:

  * `preferred-operator`: the operator character to insert when a completion is selected.
  * `disabled`: `true` if the input is to be disabled, `false` otherwise.
  * `placeholder`: placeholder text for the input field.
  * `title`: title attribute for the input field.
  * `inner-ref`: React ref for the input field element.
  * `on-change`: callback to be invoked on query change, with the query and completion context, respectively.
  * `on-submit`: callback to be invoked on submit, and is invoked with the query submitted.
  * `on-context-change`: callback to be invoked with the completion context when that context changes.
  * `wrapper-base`: HTML element to contain the typeahead-input.
  * `input-field-base`: HTML element to act as the base input field element.
  * `completion-list-base`: HTML element to contain the completion list.
  * `completion-item-base`: HTML element to contain each completion item."
  []
  (let [state (r/atom nil)]
    (fn [{:keys [idx query preferred-operator
                 disabled placeholder title inner-ref
                 on-change on-submit on-context-change
                 wrapper-base input-field-base
                 completion-list-base completion-item-base]}]
      [typeahead/component {:query (or query "")
                            :completions (complete state idx)

                            :preferred-operator (or preferred-operator default-operator)
                            :disabled disabled
                            :placeholder placeholder
                            :title title
                            :inner-ref inner-ref

                            :on-change (make-on-change-handler state on-change)
                            :on-submit (or on-submit identity)
                            :on-context-change (make-on-context-change-handler state on-context-change)

                            :completion-item-component completion-item/component
                            :completion-list-component completion-list/component
                            :input-field-component input-field/component

                            :input-field-base input-field-base
                            :completion-list-base completion-list-base
                            :completion-item-base completion-item-base}])))
