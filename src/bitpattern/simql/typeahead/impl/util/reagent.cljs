(ns bitpattern.simql.typeahead.impl.util.reagent
  (:require [reagent.core :as r]))

(defn component-did-x-props
  [f]
  (fn [this & old-argv]
    (let [props (second (r/argv this))
          old-props (second (first old-argv))]
      (f this props old-props))))
