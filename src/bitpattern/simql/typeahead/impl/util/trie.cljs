(ns bitpattern.simql.typeahead.impl.util.trie
  (:require [clojure.string :as cstr]))

(defn ->basic-entry
  [term]
  {:term term
   :props {}})

(defn- into-index
  "Add a list of characters to an index, implemented as a trie."
  [idx cs props]
  (if (empty? cs)
    (if (empty? idx)
      props
      (merge idx props))
    (let [c (first cs)
          next (into-index (get idx c) (rest cs) props)]
      (assoc (dissoc idx :props) c
             (if (contains? (get idx c) :props)
               (merge (get idx c) next)
               next)))))

(defn index-single
  "Given an existing index, add a new word."
  [idx {:keys [term props]}]
  (into-index idx (seq (cstr/lower-case (str term))) {:props props}))

(defn- transforming-index
  ([t transformer phrases]
   (->> phrases
        (mapcat transformer)
        distinct
        (reduce index-single t))))

(defn phrase-index*
  "Index a list of phrases with properties, returning the result as a trie."
  [t term-props]
  (transforming-index t #(list %) term-props))

(defn phrase-index
  "Index a list of phrases, returning the result as a trie."
  [t phrases]
  (phrase-index* t (map #(hash-map :term % :props {}) phrases)))

(defn- match-builder
  [data]
  (let [r (->> data
               keys
               (mapcat #(map (fn [x] (str % x)) (match-builder (get data %)))))]
    (if (contains? data :props)
      (conj r "")
      r)))

(defn- search-reducer
  [acc s]
  (let [r (get acc s)]
    (if (nil? r)
      (reduced {})
      r)))

(defn complete
  "Return all possible completions given the index data and a prefix."
  [data prefix]
  (->> prefix
       cstr/lower-case
       seq
       (reduce search-reducer data)
       match-builder
       (map #(str prefix %))
       (sort #(compare (cstr/lower-case %1) (cstr/lower-case %2)))))

(defn get-prop
  [t k]
  (get-in t [:props k]))

(defn prop-value
  [f tree ks]
  (loop [t tree
         [k & remkeys] ks
         lastv nil]
    (if (and t k)
      (recur (get t k) remkeys
             (let [v (f t k)]
               (if (nil? v) lastv v)))
      (let [v (f t k)]
        (if (nil? v) lastv v)))))

(def empty-trie
  {})
