(ns bitpattern.simql.typeahead.impl.util.completion
  (:require [bitpattern.simql.typeahead.impl.util.memo :as memo]
            [bitpattern.simql.parser.introspection :as introspect]))

(def completable-types #{:string-search :field :field-string})
(defn completable-context?
  [ctx]
  (completable-types (:type ctx)))

(def coerce-type
  {:string-search :field})

(def operator?
  (memo/with-memo
    (fn [query point]
      (let [ctx (introspect/describe-point query point)]
        (= (:type ctx) :operator)))))

(def describe-point
  (memo/with-memo
    (fn [query selection-start]
      (try
        (let [ctx (introspect/describe-point query selection-start)]
          (cond
            (completable-context? ctx)
            {:type (let [t (:type ctx)]
                     (or (coerce-type t) t))
             :string-to-point (:string-to-point ctx)
             :context (:context ctx)}

            (= (:type ctx) :operator)
            {:type :field-string
             :string-to-point ""
             :context (:context ctx)}))
        (catch js/Error e
          nil)))))

(defn- next-point
  [query point]
  (->> point
       (introspect/describe-point query)
       :span
       second
       inc))

(def has-operator?
  (memo/with-memo
    (fn [query point]
      (try
        (if-let [np (next-point query point)]
          (= (:type (introspect/describe-point query np)) :operator))
        (catch js/Error e
          nil)))))

(defn completable-query?
  [query ss]
  (boolean (describe-point query ss)))

(defn- splice-string
  [s start end text]
  (str
   (subs s 0 start)
   text
   (subs s end)))

(defn- update-query
  ([query text]
   (let [l (count query)]
     (update-query query text l)))
  ([query text start]
   (update-query query text start start))
  ([query text start end]
   (splice-string query start end text)))

(defn insert
  [selection-start selection-end query text]
  (let [ctx (introspect/describe-point query selection-start)]
    (if (and (= selection-start selection-end)
             (completable-context? ctx))
      (update-query query text
                    (get-in ctx [:span 0])
                    (get-in ctx [:span 1]))
      (update-query query text selection-start selection-end))))
