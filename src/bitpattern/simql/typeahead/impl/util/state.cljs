(ns bitpattern.simql.typeahead.impl.util.state)

(defn last-action
  [state]
  (:last-action @state))

(defn last-action-causes-dismissal?
  [state]
  (#{:dismiss-completions :input-keypress :insert-completion :blurred :submission}
   (last-action state)))

(defn record-action!
  [state action]
  (swap! state assoc :last-action action))

(defn- select-completion!
  [state f]
  (swap! state update :completion-idx f))

(defn set-completion-idx!
  [state v]
  (swap! state assoc :completion-idx v))

(defn set-completion-context!
  [state ctx]
  (swap! state assoc :completion-context ctx))

(defn completion-context
  [state]
  (:completion-context @state))

(defn completion-idx
  [state]
  (:completion-idx @state))

(defn select-completion-up!
  [state]
  (letfn [(decf [n]
            (let [idx (.max js/Math ((fnil dec 0) n) -1)]
              (when (> idx -1)
                idx)))]
    (select-completion! state decf)
    (record-action! state :select-completion)))

(defn select-completion-down!
  [state max-idx]
  (letfn [(incf [n]
            (.min js/Math ((fnil inc -1) n) max-idx))]
    (select-completion! state incf)
    (record-action! state :select-completion)))

(defn dismiss-completions!
  [state]
  (select-completion! state (constantly nil))
  (record-action! state :dismiss-completions))

(defn focused?
  [state]
  (:focused? @state))

(defn focused!
  [state]
  (swap! state assoc :focused? true)
  (record-action! state :focus))

(defn blurred!
  [state]
  (swap! state assoc :focused? false)
  (record-action! state :blurred))

(defn input-ref
  [instance-state]
  (:input-ref @instance-state))

(defn input-selection-start
  [instance-state]
  (if-let [iref (input-ref instance-state)]
    (.-selectionStart iref)))

(defn input-selection-end
  [instance-state]
  (if-let [iref (input-ref instance-state)]
    (.-selectionEnd iref)))

(defn last-selection-start
  [instance-state]
  (:last-selection-start @instance-state))

(defn update-last-selection-start!
  [instance-state]
  (swap! instance-state assoc :last-selection-start
         (input-selection-start instance-state)))

(defn input-set-point!
  [instance-state ss]
  (if-let [iref (input-ref instance-state)]
    (do
      (.blur iref)
      (js/requestAnimationFrame
       #(do
          (.setSelectionRange iref ss ss)
          (.focus iref))))))

(defn has-input-ref?
  [instance-state]
  (boolean (:input-ref @instance-state)))

(defn focus-input!
  [instance-state]
  (.focus (:input-ref @instance-state)))

(defn prev-show-completions?
  [instance-state]
  (:prev-show-completions? @instance-state))

(defn set-prev-show-completions!
  [instance-state show?]
  (swap! instance-state assoc :prev-show-completions? show?))

(defn set-ref!
  [instance-state el]
  (swap! instance-state assoc :ref el))

(defn ref
  [instance-state]
  (:ref @instance-state))

(defn is-active?
  [instance-state]
  (if-let [ref (:ref @instance-state)]
    (.contains ref (.-activeElement js/document))))

(defn query
  [instance-state]
  (:query @instance-state))

(defn set-query!
  [instance-state query]
  (swap! instance-state assoc :query query))

(defn completions
  [instance-state]
  (:completions @instance-state))

(defn set-completions!
  [instance-state completions]
  (swap! instance-state assoc :completions completions))

(defn latest-query
  [instance-state]
  (:latest-query @instance-state))

(defn set-latest-query!
  [instance-state latest-query]
  (swap! instance-state assoc :latest-query latest-query))

(defn set-auto-insert!
  [instance-state s]
  (swap! instance-state assoc :auto-insert s))

(defn auto-insert
  [instance-state s]
  (swap! instance-state assoc :auto-insert s))
