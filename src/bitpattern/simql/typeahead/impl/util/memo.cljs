(ns bitpattern.simql.typeahead.impl.util.memo
  (:require [cljs.cache :as cache]))

(defn with-memo
  [f & opts]
  (let [c (atom (cache/lru-cache-factory {} :threshold 16))]
    (fn [& args]
      (let [v @c]
        (if (cache/has? v args)
          (do
            (reset! c (cache/hit v args))
            (cache/lookup v args))
          (let [r (apply f args)]
            (reset! c (cache/miss v args r))
            r))))))
