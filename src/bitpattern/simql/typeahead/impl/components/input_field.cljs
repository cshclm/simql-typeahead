(ns bitpattern.simql.typeahead.impl.components.input-field
  (:require [bitpattern.simql.typeahead.impl.components.styles :as styles]
            [bitpattern.simql.typeahead.impl.util.state :as state]
            [bitpattern.simql.typeahead.impl.util.reagent :as ur]
            [bitpattern.simql.typeahead.impl.util.memo :as memo]
            [reagent.core :as r]))

(def modifier-key? #{"Alt" "AltGraph" "CapsLock" "Control" "Fn" "Hyper" "Meta"
                     "NumLock" "OS" "Scroll" "ScrollLock" "Shift" "Super"
                     "Symbol" "SymbolLock" "Win"})

(def make-key-down-handler
  (memo/with-memo
    (fn [state]
      (fn [evt]
        (when-not (or (modifier-key? (.-key evt))
                      (= (.-key evt) "Enter"))
          (state/record-action! state :input-keypress))))))

(def make-change-handler
  (memo/with-memo
   (fn [state instance-state query on-change]
     (fn [evt]
       (when on-change
         (on-change (.. evt -target -value))
         (state/record-action! state :query-changed))))))

(def make-ref-handler
  (memo/with-memo
   (fn [instance-state inner-ref]
     (fn [el]
       (when inner-ref
         (inner-ref el))
       (swap! instance-state assoc :input-ref el)))))

(defn- splice
  [base ins pos]
  (str (subs base 0 pos)
       ins
       (subs base (inc pos))))

(def input-component
  (with-meta
    (fn [{:keys [state instance-state query disabled
                 placeholder title inner-ref
                 should-focus? base-component on-change on-click]
          :as props}]
      [base-component {:value (or query "")
                       :disabled disabled
                       :placeholder placeholder
                       :title title
                       :on-change (make-change-handler state instance-state query on-change)
                       :onKeyDown (make-key-down-handler state)
                       :onClick on-click
                       :ref (make-ref-handler instance-state inner-ref)}])
    {:component-did-update
     (ur/component-did-x-props
      (fn [this {:keys [should-focus? instance-state on-change query]} old-props]
        (if-let [c (-> instance-state deref :auto-insert)]
          (if-let [on-change on-change]
            (let [ss (state/input-selection-start instance-state)]
              (on-change (splice query c ss))
              (state/set-auto-insert! instance-state nil)
              (state/input-set-point! instance-state ss))))
        (when (and should-focus? (not (:should-focus? old-props)))
          (.focus (r/dom-node this)))))}))

(defn component
  [props]
  (let [props-with-defaults
        (update props :base-component #(or % styles/default-input-field))]
    [input-component props-with-defaults]))
