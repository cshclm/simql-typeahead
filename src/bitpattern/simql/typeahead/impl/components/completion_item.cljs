(ns bitpattern.simql.typeahead.impl.components.completion-item
  (:require [bitpattern.simql.typeahead.completion :as completion]
            [bitpattern.simql.typeahead.impl.components.styles :as styles]
            [bitpattern.simql.typeahead.impl.util.reagent :as ur]
            [reagent.core :as r]))

(def completion-item-component
  (with-meta
    (fn [{:keys [idx base-component completion-component text
                 type subcontext selected? focused? on-insert]}]
      [:li {:style {:outline "none"}
            :role "option"
            :tabIndex "-1"
            :onClick #(on-insert text)
            :onKeyDown #(when (= (.-key %) "Enter")
                          (.stopPropagation %)
                          (on-insert text))}
       [base-component {:selected? selected?
                        :focused? focused?
                        :tabIndex "-1"
                        :type type
                        :subcontext subcontext
                        :value text}
        text]])
    {:component-did-mount
     (ur/component-did-x-props
      (fn [this {:keys [focused?]}]
        (when focused?
          (.focus (r/dom-node this)))))
     :component-did-update
     (ur/component-did-x-props
      (fn [this {:keys [focused?]} {prev-focused? :focused?}]
        (when (and focused? (not prev-focused?))
          (.focus (r/dom-node this)))))}))

(defn component
  [props]
  (let [props-with-defaults
        (update props :base-component
                #(or % styles/default-completion-item))]
    [completion-item-component props-with-defaults]))
