(ns bitpattern.simql.typeahead.impl.components.typeahead
  (:require [bitpattern.simql.typeahead.impl.components.styles :as styles]
            [bitpattern.simql.typeahead.impl.util.completion :as completion]
            [bitpattern.simql.typeahead.impl.util.state :as state]
            [bitpattern.simql.typeahead.impl.util.reagent :as ur]
            [bitpattern.simql.typeahead.impl.util.memo :as memo]
            [reagent.core :as r]))

(defn show-completions?
  [state]
  (and (state/focused? state)
       (not (state/last-action-causes-dismissal? state))
       (state/completion-context state)))

(defn completions-visible?
  [state instance-state]
  (let [completions (state/completions instance-state)]
    (and (seq completions)
         (show-completions? state)
         (state/completion-context state))))

(defn handle-completion-change!
  [state instance-state max-idx evt]
  (cond
    (or (= (.-key evt) "ArrowUp")
        (= (.-key evt) "Up"))
    (do (.preventDefault evt)
        (.stopPropagation evt)
        (state/select-completion-up! state)
        true)

    (or (= (.-key evt) "ArrowDown")
        (= (.-key evt) "Down"))
    (do (.preventDefault evt)
        (.stopPropagation evt)
        (state/select-completion-down! state max-idx)
        true)

    (= (.-key evt) "Escape")
    (do (state/dismiss-completions! state)
        (state/focus-input! instance-state)
        (.stopPropagation evt)
        true)))

(defn handle-default-insertion!
  [state instance-state on-insert evt]
  (when (and (= (.-key evt) "Enter")
             (completions-visible? state instance-state))
    (let [text (first (state/completions instance-state))]
      (on-insert text))))

(defn handle-submission!
  [state instance-state on-submit evt]
  (let [query (state/query instance-state)]
    (when (and (= (.-key evt) "Enter")
               (not (completions-visible? state instance-state)))
      (on-submit query)
      (state/record-action! state :submission))))

(def make-query-change-handler
  (memo/with-memo
   (fn [state instance-state on-change]
     (fn [query]
       (when on-change
         (if-let [ss (state/input-selection-start instance-state)]
           (let [ctx (completion/describe-point query ss)]
             (state/set-completion-context! state ctx)
             (state/set-latest-query! instance-state query)
             (on-change query ctx))))))))

(def make-context-change-handler
  (memo/with-memo
    (fn [state instance-state on-context-change]
      (fn [_]
        (if-let [ss (state/input-selection-start instance-state)]
          (let [query (state/latest-query instance-state)
                ctx (completion/describe-point query ss)]
            (state/set-completion-context! state ctx)
            (on-context-change ctx)
            (state/record-action! state :context-change)))))))

(def make-key-down-handler
  (memo/with-memo
    (fn [state instance-state on-submit on-insert]
     (fn [evt]
       (.stopPropagation evt)
       (let [max-idx (count (state/completions instance-state))]
         (or (handle-completion-change! state instance-state max-idx evt)
             (handle-submission! state instance-state on-submit evt)
             (handle-default-insertion! state instance-state
                                        on-insert evt)))))))

(def auto-closing
  {"(" ")"
   "\"" "\""})

(def make-key-press-handler
  (memo/with-memo
    (fn [state instance-state]
      (fn [evt]
        (when (and (not (.getModifierState evt "Control"))
                   (not (.getModifierState evt "Alt"))
                   (not (= (.-key evt) "Enter")))
          (if-let [kc (auto-closing (.-key evt))]
            (state/set-auto-insert! instance-state kc))
          (when (state/has-input-ref? instance-state)
            (state/focus-input! instance-state)
            (state/dismiss-completions! state)))))))

(defn with-quoting
  [s]
  (if (re-matches #".*[\"\( \)!:=\<\>\$].*" s)
    (str "\"" s "\"")
    s))

(defn insert-completion
  [instance-state preferred-operator text]
  (if-let [ss (state/input-selection-start instance-state)]
    (let [se (state/input-selection-end instance-state)
          query (state/query instance-state)
          ctx (completion/describe-point query ss)]
      (completion/insert ss se query
                         (if (and (= :field (:type ctx))
                                  (not (completion/has-operator? query ss)))
                           (str text preferred-operator)
                           (with-quoting text))))))

(def make-insert-handler
  (memo/with-memo
    (fn [state instance-state preferred-operator handle-query-change]
     (fn [text]
       (when (state/has-input-ref? instance-state)
         (->> text
              (insert-completion instance-state preferred-operator)
              handle-query-change)
         (state/focus-input! instance-state)
         (state/record-action! state :insert-completion))))))

(defn update-completion-context
  [state instance-state on-context-change]
  (if-let [ss (state/input-selection-start instance-state)]
    (let [query (state/latest-query instance-state)
          ctx (completion/describe-point query ss)]
      (state/set-completion-context! state ctx)
      (on-context-change ctx))))

(defn update-completion-idx
  [state completions]
  (let [cidx (state/completion-idx state)]
    (if (and (int? cidx) (>= cidx (count completions)))
      (if (seq completions)
        (state/set-completion-idx! state (dec (count completions)))
        (state/set-completion-idx! state nil)))))

(defn will-focus-in-tree?
  [instance-state evt]
  (or (nil? (.-relatedTarget evt))
      (not (.contains (state/ref instance-state)
                      (.-relatedTarget evt)))))

(defn typeahead-input-component
  [{:keys [on-context-change]}]
  (let [state (r/atom {:completion-idx nil
                       :focused? false})
        instance-state (atom {:ref nil
                              :input-ref nil})
        handle-focus-in #(when-not (state/focused? state)
                           (state/focused! state))
        handle-focus-out #(when (will-focus-in-tree? instance-state %)
                            (state/dismiss-completions! state)
                            (state/blurred! state)
                            (on-context-change nil))]
    (r/create-class
     {:display-name "typeahead-input-internal"

      :reagent-render
      (fn [{:keys [query on-change on-submit completions
                   preferred-operator disabled placeholder
                   title inner-ref

                   input-field-component
                   completion-list-component
                   completion-item-component

                   input-field-base wrapper-base
                   completion-list-base completion-item-base]}]
        (state/set-query! instance-state query)
        (state/set-completions! instance-state completions)
        (let [handle-query-change
              (make-query-change-handler state instance-state on-change)

              handle-context-change
              (make-context-change-handler state instance-state on-context-change)

              handle-insert
              (make-insert-handler state instance-state preferred-operator
                                   handle-query-change)

              handle-key-down
              (make-key-down-handler state instance-state on-submit handle-insert)

              handle-key-press
              (make-key-press-handler state instance-state)]
          [wrapper-base {:onKeyDown handle-key-down
                         :onKeyPress handle-key-press
                         :onFocus handle-focus-in
                         :onBlur handle-focus-out}
           [input-field-component {:base-component input-field-base
                                   :on-change handle-query-change
                                   :on-click handle-context-change
                                   :disabled disabled
                                   :placeholder placeholder
                                   :title title
                                   :inner-ref inner-ref
                                   :query query
                                   :state state
                                   :instance-state instance-state
                                   :should-focus? (nil? (state/completion-idx state))}]
           (when (and (not disabled)
                      (completions-visible? state instance-state))
             [completion-list-component {:item-component completion-item-component
                                         :base-component completion-list-base
                                         :item-base-component completion-item-base
                                         :type (:type (state/completion-context state))
                                         :subcontext (:context (state/completion-context state))
                                         :completions completions
                                         :completion-idx (state/completion-idx state)
                                         :on-insert handle-insert}])]))

      :component-did-mount
      (fn [this]
        (state/set-ref! instance-state (r/dom-node this))

        ;; Check focus state at the next tick to allow other handlers to run
        (js/setTimeout
         #(if (state/is-active? instance-state)
            (handle-focus-in))
         0))

      :component-did-update
      (ur/component-did-x-props
       (fn [this {:keys [completions]}]
         (when-not (= (state/input-selection-start instance-state)
                      (state/last-selection-start instance-state))
           (update-completion-context state instance-state on-context-change)
           (state/update-last-selection-start! instance-state))
         (update-completion-idx state completions)))

      :component-will-unmount
      (fn []
        (state/set-ref! instance-state nil))})))

(defn component
  [props]
  (let [props-with-defaults
        (update props :wrapper-base #(or % styles/default-typeahead-input))]
    [typeahead-input-component props-with-defaults]))
