(ns bitpattern.simql.typeahead.impl.components.styles
  (:require [cljss.reagent :refer-macros [defstyled]]
            [reagent.core :as r]
            [reagent.dom :as rdom]))

(defstyled default-completion-item :div
  {:outline 0
   :display "block"
   :color "#333"
   :border-bottom "1px solid #eee"
   :padding "0.4rem 0.4rem"
   :focused? {:background-color "#88d9ff"}
   :selected? {:background-color "#eee"}
   :&:hover {:background-color "#ddd"}})

(defn default-input-field
  [props]
  [:input (merge {:style {:padding "0.4rem"
                          :border-radius "0.2rem"
                          :border "1px solid #ccc"
                          :width "20rem"}}
                 props)])

(defstyled default-completion-list :ul
  {:position "absolute"
   :background "white"
   :z-index 1000
   :list-style "none"
   :padding 0
   :border "1px solid #eee"
   :min-width "300px"
   :margin-top "0.2rem"})

(def default-typeahead-input :span)
