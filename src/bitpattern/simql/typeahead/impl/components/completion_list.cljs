(ns bitpattern.simql.typeahead.impl.components.completion-list
  (:require [bitpattern.simql.typeahead.impl.components.styles :as styles]))

(defn completion-list-component
  [{:keys [base-component item-component item-base-component
           type subcontext completions completion-idx on-insert]}]
  [base-component
   (map-indexed
    (fn [i c]
      ^{:key c}
      [item-component
       {:base-component item-base-component
        :text c
        :type type
        :subcontext subcontext
        :on-insert on-insert
        :focused? (= completion-idx i)
        :selected? (or (= completion-idx i)
                       (and (nil? completion-idx)
                            (zero? i)))}])
    completions)])

(defn component
  [props]
  (let [props-with-defaults
        (update props :base-component
                #(or % styles/default-completion-list))]
    [completion-list-component props-with-defaults]))
