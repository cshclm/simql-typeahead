(ns bitpattern.simql.typeahead.completion
  "Datastructure and protocols for offering typeahead completions."
  (:require [bitpattern.simql.typeahead.impl.util.memo :as memo]
            [bitpattern.simql.typeahead.impl.util.trie :as trie]
            [reagent.core :as r]))

(def default-sort-fn identity)
(def default-limit 10)

(defprotocol CompletionIndex
  "Protocol for context-sensitive typeahead completions."
  (complete [this ctx]
    "Return a completions for the current index state and a given context.")
  (prop [this ctx k]
    "Retrieve the value of a property for a context.")
  (index! [this ctx completions]
    "Add the completions to a given context.")
  (set-props! [this ctx props]
    "Add properties to a given context."))

(defn- get-props
  [m]
  (get @m ::props))

(defn- get-props-ctx
  [m {:keys [type context]}]
  (get-in (get-props m) [type context]))

(defn- trie-apply!
  [m k {:keys [type context]} f & args]
  (apply swap! m update-in [k type context] f args)
  m)

(defn- index-props
  [t {:keys [string-to-point]} props]
  (let [record (list {:term string-to-point
                      :props props})]
    (trie/phrase-index* (or t trie/empty-trie) record)))

(defn- set-props!*
  [m ctx props]
  (trie-apply! m ::props ctx index-props ctx props))

(defn- index-completions
  [t completions]
  (trie/phrase-index (or t trie/empty-trie) completions))

(defn- index!*
  [m ctx completions]
  (trie-apply! m ::completions ctx index-completions completions))

(defn- sort-fn
  [m]
  (::sort-fn (get-props m)))

(defn- sort-fn!*
  [m f]
  (swap! m assoc-in [::props ::sort-fn] f))

(defn- limit
  [m]
  (::limit (get-props m)))

(defn- apply-limit
  [m coll]
  (if-let [n (limit m)]
    (take n coll)
    coll))

(defn- limit!*
  [m n]
  (swap! m assoc-in [::props ::limit] n))

(defn- prop*
  [m {:keys [string-to-point] :as ctx} k]
  (->> string-to-point
       seq
       (trie/prop-value (fn [t _] (trie/get-prop t k))
                        (get-props-ctx m ctx))))

(def ^:private complete-subtree
  (memo/with-memo
    (fn [m subt string-to-point]
      (->> ""
           (trie/complete subt)
           (map #(str string-to-point %))
           (remove #(= string-to-point %))
           (sort-by (sort-fn m))
           (apply-limit m)))))

(defn- complete*
  [m {:keys [string-to-point type context]}]
  (let [subt @(r/cursor m (apply vector ::completions type context (seq string-to-point)))]
    (complete-subtree m subt string-to-point)))

(deftype DefaultCompletionIndex [m]
  CompletionIndex
  (complete [_ ctx]
    (complete* m ctx))

  (index! [this ctx completions]
    (index!* m ctx completions)
    this)

  (prop [this ctx k]
    (prop* m ctx k))

  (set-props! [this ctx props]
    (set-props!* m ctx props)))

(defn ^:export create
  "Create a new index."
  ([]
   (create {}))
  ([{:keys [limit sort-fn]
     :or {limit default-limit
          sort-fn default-sort-fn}}]
   (DefaultCompletionIndex.
    (r/atom {::props {::sort-fn sort-fn
                      ::limit limit}}))))
