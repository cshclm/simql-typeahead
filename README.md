# SimQL Typeahead

A React/Reagent component providing typeahead for SimQL, a Simple Query Language.

![](doc/sample.png)

You will likely also be interested in [SimQL Parser](https://gitlab.com/cshclm/simql-parser/).

## Installation

`deps.edn`:
```clj
{:deps
 {camelot/simql-parser {:git/url "https://gitlab.com/cshclm/simql-typeahead"
                        :sha "<LATEST COMMIT HASH>"}
 ...}}
```

## Usage

Below is an example usage in a Reagent application:

```clj
(ns your.namespace
  (:require [bitpattern.simql.typeahead.core :as typeahead]
            [bitpattern.simql.typeahead.completion :as completion]))

(defn search-component
  [props]
  (let [state (r/atom {:query ""})
        idx (completion/create {:sort-fn count :limit 5})
        on-change (fn [query ctx] (println query ctx))]
    (completion/index! idx {:type :field
                            :string-to-point ""}
                       completions ["make" "model" "year"])
    (fn []
      [(typeahead/typeahead-input {:query (:query @state)
                                   :on-change on-change
                                   :on-submit #(.log js/console %)
                                   :on-context-change #(.log js/console %)
                                   :idx idx
                                   :placeholder "Search..."
                                   :title "Type your search here"})])))
```

### Index

The important thing to note here is the usage of the completion index.

```clj
(let [idx (completion/create {:sort-fn count :limit 5})] ...)
```

This creates a new index, sorted by length (`count`) and which limits the
number of results returned for a query to `5`. An index is a glorified Reagent
`atom`, which is designed to facilitate fast typeahead search with dynamic
completions.

### Adding completions

There are two sorts of completions in the typeahead: fields and
context-specific.

#### Field completions

Field completions are offered when there is no current field in context. This
when the cursor is not on a term following a field. For example, when the
typeahead field is blank.

The following snippet adds `:field` completions:

```clj
(completion/index! idx {:type :field
                        :string-to-point ""}
                   completions ["make" "model" "year"])
```

At this point a user could type `m`, and will be offered `make` and `model` as
completions.

#### Contextual completions

What we would want, is for the user to be offered not only completions on the
field, but also the possible values for that field.

The below registers 3 completions for the `make` field:

```clj
(completion/index! idx {:type :field-string
                        :context {:field "make"}
                        :string-to-point ""}
                   ["Ferrari" "Ford" "Renaut" "Volkswagon"])
```

#### Dynamic completions

When completing fields, or field values in particular, you may not know all of
the options in advance. That's okay though, because and can call `index!` at
any time, to add in completions and the component will update itself on the
fly.

As we would not want to fetch completions where they're already entirely
fetched, we leverage the ability to set properties on the completion tree via
`set-prop!`, and then read the properies with `prop`.

```clj
(defn search-component
  [props]
  (let [idx (completion/create {:sort-fn count :limit 5})
        on-change (fn [_ ctx]
                    (when-not (completion/prop idx ctx :hydrated?)
                      (if-let [field (-> ctx :context :field)]
                        (let [input (-> ctx :context :string-to-point)
                              {:keys [completions next-page]} @(fetch (format "/completions?query=%s&field=%s" input field))]
                          (completion/index! idx ctx ["person" "animal"])
                          (completion/set-props! idx ctx {:hydrated? (nil? next-page)})))))]
    ...
    (fn []
      [(typeahead/typeahead-input {:on-change on-change
                                   :idx idx
                                   ...})])))


```

This implements an `on-change` callback which may, in turn, call an endpoint
to fetch more completions. In the above example case we set and check
`hydrated?`. This property set will apply to any context more _specific_ than
the one with the property.

### Component API

The following props are required:

* `idx`: the completion index to use. See `bitpattern.simql.typeahead.completion`.
* `query`: the current query string

It may also take any of the following props:

* `preferred-operator`: the operator character to insert when a completion is selected.
* `disabled`: `true` if the input is to be disabled, `false` otherwise.
* `placeholder`: placeholder text for the input field.
* `title`: title attribute for the input field.
* `inner-ref`: React ref for the input field element.
* `on-change`: callback to be invoked on query change, with the query and completion context, respectively.
* `on-submit`: callback to be invoked on submit, and is invoked with the query submitted.
* `on-context-change`: callback to be invoked with the completion context when that context changes.
* `wrapper-base`: a custom React component to contain the typeahead-input.
* `input-field-base`: a custom React component to act as the base input field element.
* `completion-list-base`: a custom React component to contain the completion list.
* `completion-item-base`: a custom React component to contain each completion item.

## License

Copyright © 2020 Chris Mann

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
