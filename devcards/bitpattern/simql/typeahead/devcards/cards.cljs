(ns bitpattern.simql.typeahead.devcards.cards
  (:require [devcards.core :as devcards :refer [defcard-rg]]
            [bitpattern.simql.typeahead.completion :as completion]
            [bitpattern.simql.typeahead.core :as typeahead]
            [cljss.reagent :refer-macros [defstyled]]
            [reagent.core :as r]))

(defstyled custom-completion-item-wrapper :div
  {:outline 0
   :display "block"
   :color "#333"
   :border-bottom "1px solid #eee"
   :padding "0.4rem 0.4rem"
   :focused? {:background-color "#88d9ff"}
   :selected? {:background-color "#eee"}
   :&:hover {:background-color "#ddd"}})

(def custom-completion-item-styles
  (r/atom {"survey-name" {:font-weight "bold"}}))

(defn custom-completion-item
  [{:keys [selected? focused? value type subcontext selected?]}]
  [custom-completion-item-wrapper
   {:style (get @custom-completion-item-styles value)
    :selected? selected?
    :focused? focused?
    :tabIndex "-1"}
   (if (and (= type :field-string)
            (= (:field subcontext) "survey-name"))
     [:div
      [:img {:src "/icon.jpg"
             :style {:margin-top "-0.2rem"
                     :margin-bottom "-0.4rem"}
             :width 32
             :height 32}]
      [:span {:style {:position "relative"
                      :top "-4px"
                      :left "6px"}}
       (str " " value " (survey)")]]
     value)])

(defn make-on-change-handler
  [state]
  (fn [query ctx]
    (.log js/console ctx)
    (swap! state assoc :query query)))

(defn typeahead-wrapper
  [{:keys [initial-query completions survey-name-completions] :as props}]
  (let [state (r/atom {:query initial-query})
        idx (completion/create {:sort-fn count
                                :limit 5})
        on-change (make-on-change-handler state)]
    (js/setTimeout #(do
                      (completion/index! idx {:type :field
                                              :string-to-point initial-query}
                                         completions)
                      (completion/index! idx {:type :field-string
                                              :context {:field "survey-name"}
                                              :string-to-point initial-query}
                                         survey-name-completions)
                      (completion/set-props! idx
                                             {:type :field
                                              :string-to-point initial-query}
                                             {:hydrated? true}))
                   2000)
    (fn []
      [typeahead/typeahead-input (merge {:query (:query @state)
                                         :on-change on-change
                                         :on-submit #(.log js/console %)
                                         :on-context-change #(.log js/console %)
                                         :idx idx}
                                        props)])))

(defcard-rg interactive-typeahead
  [typeahead-wrapper {:initial-query ""
                      :placeholder "Search..."
                      :title "Type your search here"
                      :completions '("survey-description"
                                     "survey-id"
                                     "survey-name"
                                     "site-name"
                                     "sighting-id"
                                     "sighting-quantity"
                                     "site-id"
                                     "site-location"
                                     "site-country"
                                     "survey-description"
                                     "trap-station-name"
                                     "trap-station-id"
                                     "camera-id")}])

(defcard-rg interactive-typeahead-with-custom-items
  [typeahead-wrapper {:initial-query ""
                      :placeholder "Search..."
                      :title "Type your search here"
                      :completion-item-base custom-completion-item
                      :completions '("survey-description"
                                     "survey-id"
                                     "survey-name"
                                     "site-name"
                                     "sighting-id"
                                     "sighting-quantity"
                                     "site-id"
                                     "site-location"
                                     "site-country"
                                     "survey-description"
                                     "trap-station-name"
                                     "trap-station-id"
                                     "camera-id")
                      :survey-name-completions '("Survey 1"
                                                 "Survey 2")}])

(defcard-rg interactive-fields-equals-typeahead
  [typeahead-wrapper {:initial-query ""
                      :preferred-operator "="
                      :completions '("survey-description"
                                     "survey-id"
                                     "survey-name"
                                     "trap-station-name"
                                     "trap-station-id"
                                     "camera-id")}])

(defcard-rg disabled-typeahead
  [typeahead-wrapper {:initial-query ""
                      :disabled true
                      :completions '("survey-description"
                                     "survey-id"
                                     "survey-name"
                                     "trap-station-name"
                                     "trap-station-id"
                                     "camera-id")}])
