(ns ^:figwheel-hooks bitpattern.simql.typeahead.devcards.core
  (:require [devcards.core :as devcards]
            [bitpattern.simql.typeahead.devcards.cards]))

(enable-console-print!)

(defn render []
  (devcards/start-devcard-ui!))

(defn ^:after-load render-on-relaod []
  (render))

(render)
